+++
date = "2023-04-11"
draft = false
title = "Welcome"
framed = true
+++

# Hi there
Hello, my name is Adrian. I'm an electronic engineer working on differents kind of cool stuff.
I supported some artists with my technical skills to help them achieved their artistic projects. You can also find on this page some DIY and Open Source synth/instrument.

# Hey Coucou
Bonjour à tous

Bonjour, je m'appelle Adrian. Je suis ingénieur en électronique et je travaille sur différents types de dispositifs cool. J'ai accompagné quelques artistes avec mes compétences techniques pour les aider à réaliser leurs projets artistiques. Vous trouverez également sur cette page des synthés/instruments DIY et Open Source.