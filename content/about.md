+++
title = "About"
date = "2023-04-11"
+++

# Hi there
Welcome to you, my name is Adrian. I'm an electronic engineer working on differents kind of cool stuff.
I supported some artists with my technical skills to help them achieved their artistic projects.
I also make some DIY and Open Source synth/instrument.

You can find all my OpenSource project on my [**_gitlab_**](https://gitlab.com/vr0um).

You can send me an email to [**_say hello_**]("mailto:adrian.badoil@protonmail.com")

I also have [**_instragram_**](https://instagram.com/vr0um) for other stuff.

# Salut !
Soyez les bienvenus, je m'appelle Adrian. Je suis ingénieur en électronique et je travaille sur différents types de dispositifs cool.
J'ai accompagné quelques artistes avec mes compétences techniques pour les aider à réaliser leurs projets artistiques.
Je fabrique également des synthés/instruments DIY et Open Source.

Vous pouvez trouver tous mes projets OpenSource sur mon [**_gitlab_**](https://gitlab.com/vr0um).

Vous pouvez m'envoyer un email à [**_say hello_**]("mailto:adrian.badoil@protonmail.com")

J'ai aussi [**_instragram_**](https://instagram.com/vr0um) pour d'autres choses.

Traduit avec www.DeepL.com/Translator (version gratuite)