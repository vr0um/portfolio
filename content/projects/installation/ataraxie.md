+++
title = "Ataraxie"
date = "2023-04-01"
portfolioCover = "img/ataraxie.png"
portfolioIcons=["icon/laser.svg"]
tags= ["light", "motorisation"]
weight = 1
cover = "img/ataraxie.png"
+++

Ataraxie est une installation immersive.

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/696132034?h=1c2d30556f" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<p><a href="https://vimeo.com/696132034">Ataraxie</a> from <a href="https://vimeo.com/collectifcoin">Collectif Coin</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

“Ataraxie” signifie “paix de l’esprit”. Ce ballet de lumières rouges, dont les formes deviennent de plus en plus complexes au fil de la performance, nous invite à réfléchir au sens de la tranquillité dans un monde en mouvement constant.


# Rôle
Design du dispositif, gestion de projet, conception électronique et du contrôle en temps réel, régie

# Distribution
By Maxime Houot

- Production: Collectif Coin
- Engineering team : Adrian Badoil, Martin Paris
- Technical directors: Pierre Dubois, Nikola Pilepic
- Support : La Bifurk
- Premiered at Sonica by Cryptic – Glasgow (03/2022)