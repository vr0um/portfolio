+++
title = "DrumPepper Machine"
date = "2022-04-23"
portfolioCover = "img/pepper.png"
portfolioIcons=["icon/dial-high.png"]
tags=["music","instrument","DIY", "PureData", "bela"]
weight = 2
cover = "img/pepper.png"
+++

Ce projet utilise une carte [Bela](https://bela.io/) avec leur panel [Pepper](https://shop.bela.io/products/pepper) et a été assemblé par mes soins.

Il s'agit d'une drum machine pour modulaire programmé via PureData. Je me suis basé sur une base d'un exemple de cette carte. Tu peux retrouver l'[example de base ici](https://github.com/BelaPlatform/Bela/tree/master/examples/Pepper/drum-machine).
Vous pouvez retrouver le projet sur [gitlab](https://gitlab.com/vr0um1/bela-drumpepper)


## Mockup

![image alt pepper mockup](/img/pepper_mockup.png)

## Spec

* 4 drums channels
* pitch modifiable sur chaque sample
* Une reverb cool en sortie

## Utilisation

Si vous voulez essayer ce patch, il faut mettre dans le dossier du programme vos samples avec le format de nom suivant ```x.wav``` *(x de 1 à 15)*