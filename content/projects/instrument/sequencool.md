+++
title = "Sequencool"
date = "2021-06-23"
portfolioCover = "img/sequencool.jpg"
portfolioIcons=["icon/dial-high.png"]
tags=["music","instrument","DIY"]
weight = 1
cover = "img/sequencool.jpg"
+++

Le ***```sequencool```*** est un séquenceur modulaire conçu avec et pour l'artiste numérique [***```marylou```***](https://maryloupetot.com).

Il s'agit d'un séquenceur réalisé sur Arduino et opensource. Vous pouvez retrouver le projet sur [***```gitlab```***](https://gitlab.com/marylougarou/sequencool)

## Mockup

![image alt sequencool mockup](/img/sequencool_mockup.png)

## Spec

* 8/16/24/32 pas
* 6 banks de mémoire
* 8 sorties
* une entrée clock-in et synch
* un mode random